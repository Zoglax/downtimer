package com.example.timerdown;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    TextView textViewTime, textViewTimeIsStop;
    Button buttonDown;
    EditText editTextSec, editTextMin;

    Timer timer;
    TimerTask timerTask;

    int currentSeconds=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTime = findViewById(R.id.textViewTime);
        textViewTimeIsStop = findViewById(R.id.textViewTimeIsStop);

        buttonDown = findViewById(R.id.buttonDown);

        editTextSec = findViewById(R.id.editTextSec);
        editTextMin = findViewById(R.id.editTextMin);

        timer = null;
        timerTask = null;

        buttonDown.setOnClickListener(buttonDownOnClick);
    }

    View.OnClickListener buttonDownOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            timer = new Timer();

            int sec = Integer.parseInt(editTextSec.getText().toString());
            int min = Integer.parseInt(editTextMin.getText().toString());

            currentSeconds = min * 60 + sec;

            timerTask = new TimerTask() {
                @Override
                public void run()
                {
                    currentSeconds--;
                    int sec = currentSeconds%60;
                    int min = currentSeconds/60;

                    final String output = String.format("%02d:%02d",min,sec);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run()
                        {
                            textViewTime.setText(output);
                        }
                    });

                    if (currentSeconds==0)
                    {
                        timer.cancel();
                        timer = null;

                        textViewTime.setText("00:00");
                        textViewTimeIsStop.setText("Time is stop!!!");
                    }
                }
            };

            timer.schedule(timerTask,1000, 1000);

            buttonDown.setEnabled(false);
        }

    };
}
